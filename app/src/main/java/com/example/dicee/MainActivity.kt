package com.example.dicee

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val diceImage: ImageView = findViewById(R.id.dice_image)

        val rollBtn: Button = findViewById(R.id.roll_button)
        rollBtn.setOnClickListener {
            val randomInt = (1..6).random()
            val drawableResource = when (randomInt) {
                1 -> R.drawable.dice1
                2 -> R.drawable.dice2
                3 -> R.drawable.dice3
                4 -> R.drawable.dice4
                5 -> R.drawable.dice5
                else -> R.drawable.dice6
            }
            diceImage.setImageResource(drawableResource)
        }

        val clearbtn:Button = findViewById(R.id.clear_button)
        clearbtn.setOnClickListener {
            diceImage.setImageResource(R.drawable.empty_dice)
        }
    }


}
